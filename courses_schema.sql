# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.28)
# Database: courses
# Generation Time: 2021-07-25 12:29:08 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table branches
# ------------------------------------------------------------

DROP TABLE IF EXISTS `branches`;

CREATE TABLE `branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `branch_courses_key` (`course_id`),
  KEY `branch_users_key` (`user_id`),
  CONSTRAINT `branch_courses_key` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  CONSTRAINT `branch_users_key` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `branches` WRITE;
/*!40000 ALTER TABLE `branches` DISABLE KEYS */;

INSERT INTO `branches` (`id`, `created_at`, `updated_at`, `user_id`, `course_id`, `name`)
VALUES
	(1,'2021-07-25 10:58:21','2021-07-25 10:58:21',2,1,'db_rails'),
	(2,'2021-07-25 10:58:36','2021-07-25 10:58:36',2,1,'db_rails'),
	(3,'2021-07-25 10:59:07','2021-07-25 10:59:07',2,1,'db_rails'),
	(4,'2021-07-25 10:59:34','2021-07-25 10:59:34',2,1,'db_rails'),
	(5,'2021-07-25 10:59:07','2021-07-25 10:59:07',2,1,'master'),
	(6,'2021-07-25 12:16:40','2021-07-25 12:16:40',2,3,'master'),
	(7,'2021-07-25 12:18:15','2021-07-25 12:18:15',2,3,'learn_controller_branch');

/*!40000 ALTER TABLE `branches` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table courses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `courses`;

CREATE TABLE `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `label` varchar(256) DEFAULT NULL,
  `is_visible` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;

INSERT INTO `courses` (`id`, `created_at`, `updated_at`, `label`, `is_visible`)
VALUES
	(1,'2021-05-26 05:48:06','2021-05-26 05:48:06','sample course 1',1),
	(2,'2021-05-26 05:48:06','2021-05-26 05:48:06',' course 2',1),
	(3,'2021-07-25 12:16:40','2021-07-25 12:16:40','A course on how to create Rails web app',0);

/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table lessons
# ------------------------------------------------------------

DROP TABLE IF EXISTS `lessons`;

CREATE TABLE `lessons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `course_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `is_visible` tinyint(1) NOT NULL DEFAULT '0',
  `label` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `course_lesson_key` (`course_id`),
  KEY `user_lesson_key` (`user_id`),
  KEY `branch_lesson_key` (`branch_id`),
  CONSTRAINT `branch_lesson_key` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`),
  CONSTRAINT `course_lesson_key` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  CONSTRAINT `user_lesson_key` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `lessons` WRITE;
/*!40000 ALTER TABLE `lessons` DISABLE KEYS */;

INSERT INTO `lessons` (`id`, `created_at`, `updated_at`, `course_id`, `user_id`, `branch_id`, `is_visible`, `label`)
VALUES
	(1,'2021-07-25 11:34:46','2021-07-25 11:34:46',1,2,4,1,'database access'),
	(2,'2021-07-25 11:37:10','2021-07-25 11:37:10',1,2,4,1,'database access'),
	(3,'2021-07-25 11:38:52','2021-07-25 11:38:52',1,2,4,1,'sample create query'),
	(4,'2021-07-25 11:39:03','2021-07-25 11:39:03',1,2,4,1,'sample create query'),
	(5,'2021-07-25 12:20:29','2021-07-25 12:20:29',3,2,7,0,'learn GET request'),
	(6,'2021-07-25 12:20:48','2021-07-25 12:20:48',3,2,7,0,'learn POST request');

/*!40000 ALTER TABLE `lessons` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table schema_migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `schema_migrations`;

CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;

INSERT INTO `schema_migrations` (`version`)
VALUES
	('20210724193814'),
	('20210725064526'),
	('20210725082513'),
	('20210725101839'),
	('20210725101846');

/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sections`;

CREATE TABLE `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `label` varchar(256) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `lesson_id` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `s3_url` varchar(256) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `branch_section_key` (`branch_id`),
  KEY `user_section_key` (`user_id`),
  KEY `lesson_section_key` (`lesson_id`),
  CONSTRAINT `branch_section_key` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`),
  CONSTRAINT `lesson_section_key` FOREIGN KEY (`lesson_id`) REFERENCES `lessons` (`id`),
  CONSTRAINT `user_section_key` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;

INSERT INTO `sections` (`id`, `created_at`, `updated_at`, `label`, `branch_id`, `user_id`, `status`, `lesson_id`, `position`, `s3_url`, `type`)
VALUES
	(1,'2021-07-25 11:42:38','2021-07-25 11:42:38','sample create query',4,2,2,3,1,'dummy url',NULL),
	(3,'2021-07-25 11:44:51','2021-07-25 11:44:51','sample create query',4,2,2,3,2,'dummy url',NULL),
	(6,'2021-07-25 12:26:56','2021-07-25 12:26:56','VIDEO. sending params in GET request . ',7,2,1,5,1,'dummy url',NULL),
	(7,'2021-07-25 12:27:20','2021-07-25 12:27:20','TEXT. sending params in GET request . ',7,2,1,5,2,'dummy url',NULL),
	(8,'2021-07-25 12:28:40','2021-07-25 12:28:40','TEXT. sending params in POST request . ',7,2,1,6,1,'dummy url',NULL),
	(9,'2021-07-25 12:28:48','2021-07-25 12:28:48','VIDEO. sending params in POST request . ',7,2,1,6,2,'dummy url',NULL);

/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `name` varchar(256) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `created_at`, `updated_at`, `name`, `email`)
VALUES
	(1,'2021-05-26 05:48:06','2021-05-26 05:48:06','raghav','raghavns35@gmail.com'),
	(2,'2021-07-25 07:31:15','2021-07-25 07:31:17','raghav','raghavns35@gmail.com'),
	(3,'2021-07-25 07:31:56','2021-07-25 07:31:56','raghav','raghavns35@gmail.com'),
	(4,'2021-07-25 07:34:42','2021-07-25 07:34:42','raghav','raghavns35@gmail.com'),
	(5,'2021-07-25 07:38:46','2021-07-25 07:38:46','raghav1','raghavns35@gmail.com1'),
	(6,'2021-07-25 12:12:36','2021-07-25 12:12:36','raghavns','raghavns35@yopmail.com');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
