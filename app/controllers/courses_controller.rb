class CoursesController < ApplicationController

  def index
    sample_json = CoursesBlueprint.render_as_hash(Course.where({is_visible: true}), {view: :index})
    render json: sample_json
  end

  def show
    course_json = CoursesBlueprint.render_as_hash(Course.find(params[:id]), {view: :course_details})
    lessons_list = Lesson.where({course_id: params[:id], is_visible: true})
    course_json[:lessons] = lessons_list.map { |lesson| { id: lesson.id, label: lesson.label } }
    render json: course_json
  end

  def create_course
    new_course = Course.new
    ActiveRecord::Base.transaction do
      new_course.label = params[:label]
      new_course.save
      new_course.reload

      new_branch = Branch.new
      new_branch.course_id = new_course.id
      new_branch.user_id = params[:user_id]
      new_branch.name = 'master'
      new_branch.save
    end
    render json: CoursesBlueprint.render_as_hash(new_course, {view: :course_details})
  end

  def create_branch
    new_branch = Branch.new
    new_branch.course_id = params[:id]
    new_branch.user_id = params[:user_id]
    new_branch.name = params[:name]
    new_branch.save
    head 201
  end

  def create_lesson
    new_lesson = Lesson.new
    new_lesson.label = params[:label]
    new_lesson.course_id = params[:id]
    new_lesson.user_id = params[:user_id]
    new_lesson.branch_id = params[:branch_id]
    new_lesson.is_visible = false
    new_lesson.save
    new_lesson.reload
    head 201
  end

  def create_section
    new_section = Section.new
    new_section.label = params[:label]
    new_section.lesson_id = params[:lesson_id]
    new_section.branch_id = params[:branch_id]
    new_section.user_id = params[:user_id]
    new_section.status = Section::SECTION_STATUS[:draft]
    new_section.position = fetch_section_max_position
    new_section.type = Section::SECTION_TYPE[params[:type]]
    new_section.s3_url = 'dummy url'
    new_section.save
    head 201
  end

  def merge_branch_to_master
    # todo: move to background worker
    source_branch = Branch.find_by_name(params[:source_branch_name])
    source_branch_id = source_branch.id
    target_branch_id = Branch.where({name: 'master', course_id: source_branch.course_id}).first.id
    ActiveRecord::Base.transaction do
      # todo: batch process
      sections_list = Section.where({branch_id: source_branch_id})
      sections_list.update_all "status = #{Section::SECTION_STATUS[:merged]}, branch_id = #{target_branch_id}"
      lessons_list = Lesson.where({branch_id: source_branch_id})
      lessons_list.update_all "is_visible = true, branch_id = #{target_branch_id}"
    end
    head 200
  end

  private

  def fetch_section_max_position
    max_position = Section.where({lesson_id: params[:lesson_id]}).maximum(:position)
    max_position = 0 if max_position.nil?
    max_position + 1
  end
end
