class UsersController < ApplicationController

  def create
    new_user = User.new
    new_user.name = params[:name]
    new_user.email = params[:email]
    new_user.created_at = Time.now.utc
    new_user.updated_at = Time.now.utc
    new_user.save
    new_user.reload
    render json: UsersBlueprint.render_as_hash(new_user, view: :user_details)
  end

  def show
    render json: UsersBlueprint.render_as_hash(User.find(params[:id]), view: :user_details)
  end

  def update
    user_info = User.find(params[:id])
    user_info.email = params[:email] if params[:email]
    user_info.name = params[:name] if params[:name]
    user_info.save
    render json: UsersBlueprint.render_as_hash(user_info.reload, view: :user_details)
  end
end
