class UsersBlueprint < Blueprinter::Base
  view :user_details do
    fields :id, :name, :email
  end
end