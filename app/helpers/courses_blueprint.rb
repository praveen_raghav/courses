# frozen_string_literal: true
class CoursesBlueprint < Blueprinter::Base
  view :index do
    fields :id, :label
  end

  view :course_details do
    fields :id, :label
  end
end