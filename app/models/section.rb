class Section < ActiveRecord::Base
  SECTION_STATUS = {
      draft: 1,
      merged: 2
  }.freeze

  SECTION_TYPE = {
      text: 1,
      image: 2,
      audio: 3,
      video: 4
  }.freeze

end
